﻿namespace AplicacionBit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFechaFacturacion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.buttonEditar = new System.Windows.Forms.Button();
            this.buttonAgregar = new System.Windows.Forms.Button();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.textBoxEntregaPX = new System.Windows.Forms.TextBox();
            this.textBoxFacturaCoAseguro = new System.Windows.Forms.TextBox();
            this.textBoxMontoTotalFactura = new System.Windows.Forms.TextBox();
            this.textBoxNFactura = new System.Windows.Forms.TextBox();
            this.textBoxCantidad = new System.Windows.Forms.TextBox();
            this.textBoxFolioCartaPago = new System.Windows.Forms.TextBox();
            this.textBoxCliente = new System.Windows.Forms.TextBox();
            this.textBoxMontoPartida = new System.Windows.Forms.TextBox();
            this.textBoxDescripcionMedicamento = new System.Windows.Forms.TextBox();
            this.textBoxEstatus = new System.Windows.Forms.TextBox();
            this.textBoxMargen = new System.Windows.Forms.TextBox();
            this.textBoxCosto = new System.Windows.Forms.TextBox();
            this.textBoxOrdenCompra = new System.Windows.Forms.TextBox();
            this.textBoxCDOrigen = new System.Windows.Forms.TextBox();
            this.textBoxPaciente = new System.Windows.Forms.TextBox();
            this.textBoxCDDestino = new System.Windows.Forms.TextBox();
            this.textBoxProveedor = new System.Windows.Forms.TextBox();
            this.dataGridVieInformacion = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVieInformacion)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxFechaFacturacion
            // 
            this.textBoxFechaFacturacion.Location = new System.Drawing.Point(234, 26);
            this.textBoxFechaFacturacion.Name = "textBoxFechaFacturacion";
            this.textBoxFechaFacturacion.Size = new System.Drawing.Size(193, 20);
            this.textBoxFechaFacturacion.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fecha de Facturacion";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "N° Factura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(12, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Cliente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(12, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Monto Total Factura";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(12, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Factura Co Aseguro";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(12, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Folio de Carta Pago o Receta";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(12, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Entrega a PX";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(12, 229);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Cantidad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(12, 261);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(207, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Descripcion del Medicamento y/o Servicio";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(443, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Monto por Partida";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(443, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Costo";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(443, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Margen";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label13.Location = new System.Drawing.Point(443, 113);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Estatus";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label14.Location = new System.Drawing.Point(443, 145);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Orden de Compra";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label15.Location = new System.Drawing.Point(443, 173);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Proveedor";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label16.Location = new System.Drawing.Point(443, 201);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "CD Origen";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label17.Location = new System.Drawing.Point(443, 229);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Paciente";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label18.Location = new System.Drawing.Point(443, 261);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "CD Destino";
            // 
            // buttonEditar
            // 
            this.buttonEditar.Location = new System.Drawing.Point(877, 29);
            this.buttonEditar.Name = "buttonEditar";
            this.buttonEditar.Size = new System.Drawing.Size(75, 23);
            this.buttonEditar.TabIndex = 19;
            this.buttonEditar.Text = "Editar";
            this.buttonEditar.UseVisualStyleBackColor = true;
            this.buttonEditar.Click += new System.EventHandler(this.buttonEditar_Click);
            // 
            // buttonAgregar
            // 
            this.buttonAgregar.Location = new System.Drawing.Point(877, 70);
            this.buttonAgregar.Name = "buttonAgregar";
            this.buttonAgregar.Size = new System.Drawing.Size(75, 23);
            this.buttonAgregar.TabIndex = 20;
            this.buttonAgregar.Text = "Agregar";
            this.buttonAgregar.UseVisualStyleBackColor = true;
            this.buttonAgregar.Click += new System.EventHandler(this.buttonAgregar_Click);
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.Location = new System.Drawing.Point(877, 113);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(75, 23);
            this.buttonEliminar.TabIndex = 21;
            this.buttonEliminar.Text = "Eliminar";
            this.buttonEliminar.UseVisualStyleBackColor = true;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // textBoxEntregaPX
            // 
            this.textBoxEntregaPX.Location = new System.Drawing.Point(234, 194);
            this.textBoxEntregaPX.Name = "textBoxEntregaPX";
            this.textBoxEntregaPX.Size = new System.Drawing.Size(193, 20);
            this.textBoxEntregaPX.TabIndex = 22;
            this.textBoxEntregaPX.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBoxFacturaCoAseguro
            // 
            this.textBoxFacturaCoAseguro.Location = new System.Drawing.Point(234, 142);
            this.textBoxFacturaCoAseguro.Name = "textBoxFacturaCoAseguro";
            this.textBoxFacturaCoAseguro.Size = new System.Drawing.Size(193, 20);
            this.textBoxFacturaCoAseguro.TabIndex = 23;
            // 
            // textBoxMontoTotalFactura
            // 
            this.textBoxMontoTotalFactura.Location = new System.Drawing.Point(234, 110);
            this.textBoxMontoTotalFactura.Name = "textBoxMontoTotalFactura";
            this.textBoxMontoTotalFactura.Size = new System.Drawing.Size(193, 20);
            this.textBoxMontoTotalFactura.TabIndex = 24;
            // 
            // textBoxNFactura
            // 
            this.textBoxNFactura.Location = new System.Drawing.Point(234, 50);
            this.textBoxNFactura.Name = "textBoxNFactura";
            this.textBoxNFactura.Size = new System.Drawing.Size(193, 20);
            this.textBoxNFactura.TabIndex = 25;
            // 
            // textBoxCantidad
            // 
            this.textBoxCantidad.Location = new System.Drawing.Point(234, 226);
            this.textBoxCantidad.Name = "textBoxCantidad";
            this.textBoxCantidad.Size = new System.Drawing.Size(193, 20);
            this.textBoxCantidad.TabIndex = 26;
            // 
            // textBoxFolioCartaPago
            // 
            this.textBoxFolioCartaPago.Location = new System.Drawing.Point(234, 170);
            this.textBoxFolioCartaPago.Name = "textBoxFolioCartaPago";
            this.textBoxFolioCartaPago.Size = new System.Drawing.Size(193, 20);
            this.textBoxFolioCartaPago.TabIndex = 27;
            // 
            // textBoxCliente
            // 
            this.textBoxCliente.Location = new System.Drawing.Point(234, 81);
            this.textBoxCliente.Name = "textBoxCliente";
            this.textBoxCliente.Size = new System.Drawing.Size(193, 20);
            this.textBoxCliente.TabIndex = 28;
            // 
            // textBoxMontoPartida
            // 
            this.textBoxMontoPartida.Location = new System.Drawing.Point(547, 26);
            this.textBoxMontoPartida.Name = "textBoxMontoPartida";
            this.textBoxMontoPartida.Size = new System.Drawing.Size(231, 20);
            this.textBoxMontoPartida.TabIndex = 29;
            // 
            // textBoxDescripcionMedicamento
            // 
            this.textBoxDescripcionMedicamento.Location = new System.Drawing.Point(234, 258);
            this.textBoxDescripcionMedicamento.Name = "textBoxDescripcionMedicamento";
            this.textBoxDescripcionMedicamento.Size = new System.Drawing.Size(193, 20);
            this.textBoxDescripcionMedicamento.TabIndex = 30;
            // 
            // textBoxEstatus
            // 
            this.textBoxEstatus.Location = new System.Drawing.Point(547, 110);
            this.textBoxEstatus.Name = "textBoxEstatus";
            this.textBoxEstatus.Size = new System.Drawing.Size(231, 20);
            this.textBoxEstatus.TabIndex = 31;
            // 
            // textBoxMargen
            // 
            this.textBoxMargen.Location = new System.Drawing.Point(547, 81);
            this.textBoxMargen.Name = "textBoxMargen";
            this.textBoxMargen.Size = new System.Drawing.Size(231, 20);
            this.textBoxMargen.TabIndex = 32;
            // 
            // textBoxCosto
            // 
            this.textBoxCosto.Location = new System.Drawing.Point(547, 50);
            this.textBoxCosto.Name = "textBoxCosto";
            this.textBoxCosto.Size = new System.Drawing.Size(231, 20);
            this.textBoxCosto.TabIndex = 33;
            // 
            // textBoxOrdenCompra
            // 
            this.textBoxOrdenCompra.Location = new System.Drawing.Point(547, 142);
            this.textBoxOrdenCompra.Name = "textBoxOrdenCompra";
            this.textBoxOrdenCompra.Size = new System.Drawing.Size(231, 20);
            this.textBoxOrdenCompra.TabIndex = 34;
            // 
            // textBoxCDOrigen
            // 
            this.textBoxCDOrigen.Location = new System.Drawing.Point(547, 194);
            this.textBoxCDOrigen.Name = "textBoxCDOrigen";
            this.textBoxCDOrigen.Size = new System.Drawing.Size(231, 20);
            this.textBoxCDOrigen.TabIndex = 35;
            // 
            // textBoxPaciente
            // 
            this.textBoxPaciente.Location = new System.Drawing.Point(547, 226);
            this.textBoxPaciente.Name = "textBoxPaciente";
            this.textBoxPaciente.Size = new System.Drawing.Size(231, 20);
            this.textBoxPaciente.TabIndex = 36;
            // 
            // textBoxCDDestino
            // 
            this.textBoxCDDestino.Location = new System.Drawing.Point(547, 258);
            this.textBoxCDDestino.Name = "textBoxCDDestino";
            this.textBoxCDDestino.Size = new System.Drawing.Size(231, 20);
            this.textBoxCDDestino.TabIndex = 37;
            // 
            // textBoxProveedor
            // 
            this.textBoxProveedor.Location = new System.Drawing.Point(547, 170);
            this.textBoxProveedor.Name = "textBoxProveedor";
            this.textBoxProveedor.Size = new System.Drawing.Size(231, 20);
            this.textBoxProveedor.TabIndex = 38;
            // 
            // dataGridVieInformacion
            // 
            this.dataGridVieInformacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridVieInformacion.Location = new System.Drawing.Point(15, 293);
            this.dataGridVieInformacion.Name = "dataGridVieInformacion";
            this.dataGridVieInformacion.Size = new System.Drawing.Size(937, 318);
            this.dataGridVieInformacion.TabIndex = 39;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 690);
            this.Controls.Add(this.dataGridVieInformacion);
            this.Controls.Add(this.textBoxProveedor);
            this.Controls.Add(this.textBoxCDDestino);
            this.Controls.Add(this.textBoxPaciente);
            this.Controls.Add(this.textBoxCDOrigen);
            this.Controls.Add(this.textBoxOrdenCompra);
            this.Controls.Add(this.textBoxCosto);
            this.Controls.Add(this.textBoxMargen);
            this.Controls.Add(this.textBoxEstatus);
            this.Controls.Add(this.textBoxDescripcionMedicamento);
            this.Controls.Add(this.textBoxMontoPartida);
            this.Controls.Add(this.textBoxCliente);
            this.Controls.Add(this.textBoxFolioCartaPago);
            this.Controls.Add(this.textBoxCantidad);
            this.Controls.Add(this.textBoxNFactura);
            this.Controls.Add(this.textBoxMontoTotalFactura);
            this.Controls.Add(this.textBoxFacturaCoAseguro);
            this.Controls.Add(this.textBoxEntregaPX);
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.buttonAgregar);
            this.Controls.Add(this.buttonEditar);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxFechaFacturacion);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVieInformacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFechaFacturacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button buttonEditar;
        private System.Windows.Forms.Button buttonAgregar;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.TextBox textBoxEntregaPX;
        private System.Windows.Forms.TextBox textBoxFacturaCoAseguro;
        private System.Windows.Forms.TextBox textBoxMontoTotalFactura;
        private System.Windows.Forms.TextBox textBoxNFactura;
        private System.Windows.Forms.TextBox textBoxCantidad;
        private System.Windows.Forms.TextBox textBoxFolioCartaPago;
        private System.Windows.Forms.TextBox textBoxCliente;
        private System.Windows.Forms.TextBox textBoxMontoPartida;
        private System.Windows.Forms.TextBox textBoxDescripcionMedicamento;
        private System.Windows.Forms.TextBox textBoxEstatus;
        private System.Windows.Forms.TextBox textBoxMargen;
        private System.Windows.Forms.TextBox textBoxCosto;
        private System.Windows.Forms.TextBox textBoxOrdenCompra;
        private System.Windows.Forms.TextBox textBoxCDOrigen;
        private System.Windows.Forms.TextBox textBoxPaciente;
        private System.Windows.Forms.TextBox textBoxCDDestino;
        private System.Windows.Forms.TextBox textBoxProveedor;
        private System.Windows.Forms.DataGridView dataGridVieInformacion;
    }
}

